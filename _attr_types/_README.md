# List of attribute types

This is a [Jekyll Collection](https://jekyllrb.com/docs/collections/) of
data types for attributes of graphs/clusters/nodes/edges.

Each attribute type has a `name`, followed by a description of the type in
HTML.

The items are alphabetized when the page is created.

The type `<typename>` is given the anchor `k:<typename>`, so
the description can be linked with `<A HREF="k:<typename>">`
